import java.util.HashMap;
import java.util.Map;

public class Game {

    Map<Position, Symbol> cases = new HashMap<Position, Symbol>();

    private Symbol currentSymbol;


    public Game() {

        for(Position position : Position.values()) {
            cases.put(position, Symbol.EMPTY);
        }
    }

    public boolean isPlayed(Position position) {

        return cases.get(position) != Symbol.EMPTY;
    }

    public void play(Position position) {

        if (currentSymbol == Symbol.CROSS) {
            currentSymbol = Symbol.CIRCLE;
        } else {
            currentSymbol = Symbol.CROSS;
        }
        cases.put(position, currentSymbol);
    }

    public Symbol get(Position position) {
        return cases.get(position);
    }

}




