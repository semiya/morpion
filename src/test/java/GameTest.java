import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class GameTest {
    @Test
    public void ShouldNotPlayedOnTopLeft() throws Exception {
        Game game = new Game();
        assertThat(game.isPlayed(Position.TOP_LEFT)).isFalse();
    }
    @Test
    public void ShouldPlayedOnTopLeft() throws Exception {
        Game game = new Game();
        game.play(Position.TOP_LEFT);
        assertThat(game.isPlayed(Position.TOP_LEFT)).isTrue();

    }@Test
    public void ShouldPlayedOnTopAndTopLeftShouldBeNotPlayed() throws Exception {
        Game game = new Game();
        game.play(Position.TOP);
        assertThat(game.isPlayed(Position.TOP_LEFT)).isFalse();
    }
    @Test
    public void ShouldPlayedOnTwoDifferentPlaces() throws Exception {
        Game game = new Game();
        game.play(Position.TOP);
        game.play(Position.TOP_LEFT);
        assertThat(game.isPlayed(Position.TOP_LEFT )).isTrue();
        assertThat(game.isPlayed(Position.TOP)).isTrue();

    }

    @Test
    public void ShouldPlayedOnTop_Right() throws Exception {
        Game game = new Game();
        game.play(Position.TOP_RIGHT);
        assertThat(game.isPlayed(Position.TOP_RIGHT )).isTrue();

    }

    @Test
    public void FirstPlayShouldBeACross() throws Exception {
        Game game = new Game();

        game.play(Position.TOP_RIGHT);

        assertThat(game.isPlayed(Position.TOP_RIGHT )).isTrue();
        assertThat(game.get(Position.TOP_RIGHT)).isEqualTo(Symbol.CROSS);
    }

    @Test
    public void SecondPlayShouldBeCircle() throws Exception {
        Game game = new Game();
        game.play(Position.TOP_LEFT);

        game.play(Position.TOP);

        assertThat(game.isPlayed(Position.TOP)).isEqualTo(true);
        assertThat(game.get(Position.TOP)).isEqualTo(Symbol.CIRCLE);
    }

    @Test
    public void ShouldPlayedOnMiddle() throws Exception {
        Game game = new Game();

        game.play(Position.MIDDLE);

        assertThat(game.isPlayed(Position.MIDDLE)).isTrue();
    }

    @Test
    public void ShouldPlayedOnMiddleLeft() throws Exception {
        Game game = new Game();
        game.play(Position.MIDDLE_LEFT);

        assertThat(game.isPlayed(Position.MIDDLE_LEFT)).isTrue();
    }

    @Test
    public void ShouldPlayedOnMiddleRight() throws Exception {
        Game game = new Game();
        game.play(Position.MIDDLE_RIGHT);
        assertThat(game.isPlayed(Position.MIDDLE_RIGHT)).isTrue();
    }

    @Test
    public void ShouldPlayedOnBottom() throws Exception {
        Game game = new Game();
        game.play(Position.BOTTOM);
        assertThat(game.isPlayed(Position.BOTTOM)).isTrue();
    }

    @Test
    public void ShouldPlayedOnBottomRight() throws Exception {
        Game game = new Game();
        game.play(Position.BOTTOM_RIGHT);
        assertThat(game.isPlayed(Position.BOTTOM_RIGHT)).isTrue();
    }

    @Test
    public void ShouldPlayedOnBottomLeft() throws Exception {
        Game game = new Game();
        game.play(Position.BOTTOM_LEFT);
        assertThat(game.isPlayed(Position.BOTTOM_LEFT)).isTrue();

    }
}
